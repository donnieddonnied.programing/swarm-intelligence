import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.util.ArrayList;

public class SwarmIntelligence extends Application {

    private static final double HEIGHT = 800;
    private static final double WIDTH = 1000;
    private static double time;
    private Pane root = new Pane();
    GraphicsContext gc;
    static ArrayList<AbstractBee> bees = new ArrayList<>();
    static ArrayList<Hive> hives = new ArrayList<>();

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage theStage) {
        theStage.setTitle("Роевой интеллект");
        Scene theScene = new Scene(root);
        theStage.setScene(theScene);

        Canvas canvas = new Canvas(WIDTH, HEIGHT);
        root.getChildren().add(canvas);
        gc = canvas.getGraphicsContext2D();

        Hive hiveBlue = new Hive(400, 500, 0.0, RandomGenerator.getRandomDegree(), Color.BLUE);
        Hive hiveDarkBlue = new Hive(200, 300, 0.0, RandomGenerator.getRandomDegree(), Color.DARKBLUE);
        Hive hiveLightBlue = new Hive(600, 250, 0.0, RandomGenerator.getRandomDegree(), Color.LIGHTBLUE);
        hives.add(hiveBlue);
        hives.add(hiveDarkBlue);
        hives.add(hiveLightBlue);

        createBees(555);
        bees.add(hiveBlue);
        bees.add(hiveDarkBlue);
        bees.add(hiveLightBlue);

        final long startNanoTime = System.nanoTime();

        new AnimationTimer()
        {
            public void handle(long currentNanoTime)
            {
                time = (currentNanoTime - startNanoTime) / 1000000000.0;

                // отчистка экрана, строго один раз за цикл!
                gc.clearRect(0, 0, WIDTH, HEIGHT);
                gc.setFill(Color.BLACK);
                gc.fillRect(0, 0, WIDTH, HEIGHT);

                bees.stream().forEach(bee -> bee
                        .positionUpdate()
                        .checkBorderCollision(HEIGHT, WIDTH)
                        .transmitIfAvailable(bees)
                        .hiveDistanceUpdate()
                        .render(gc));
            }
        }.start();
        theStage.show();
    }

    public static void createBees(int num) {
        for (int i = 0; i < num; i++) {
            Bee bee = new Bee(RandomGenerator.getRandomCoord(WIDTH - 10),
                    RandomGenerator.getRandomCoord(HEIGHT - 10),
                    RandomGenerator.getRandomDouble(1.5, 1.9),
                    RandomGenerator.getRandomDegree(),
                    Color.WHITE);
            bees.add(bee);
        }
    }

    public static void createBees(int num, double xCoord, double yCoord, double speed, double direction, Color color) {
        for (int i = 0; i < num; i++) {
            Bee bee = new Bee(xCoord, yCoord, speed, direction, color);
            bees.add(bee);
        }
    }

    public static double getTime() {
        return time;
    }
}
