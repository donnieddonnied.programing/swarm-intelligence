import javafx.geometry.Rectangle2D;
import javafx.scene.paint.Color;

import java.util.List;

public class Bee extends AbstractBee {

    private static double COLLISION_TIMEOUT = 0.3;
    public double hiveCollisionTime = 0;
    public static int count = 0;
    public Color goToHiveWithColor = Color.BLUE;

    public Bee(double xCoord, double yCoord) {
        this(xCoord, yCoord, 1D, 0D, new Color(0, 0, 0, 1));
    }

    public Bee(double xCoord, double yCoord, double speed) {
        this(xCoord, yCoord, speed, 0D, new Color(0, 0, 0, 1));
    }

    public Bee(double xCoord, double yCoord, double speed, double direction) {
        this(xCoord, yCoord, speed, direction, new Color(0, 0, 0, 1));
    }

    public Bee(double xCoord, double yCoord, double speed, double direction, Color color) {
        super(xCoord, yCoord, speed, direction, color);
        this.setxCenter(getxCoord() + getWidth()/2);
        this.setyCenter(getyCoord() + getHeight()/2);
    }

    @Override
    public AbstractBee positionUpdate() {
        double angle = getDirection() * Math.PI / 180;
        this.setxCoord(getxCoord() + getSpeed() * Math.cos(angle));
        this.setyCoord(getyCoord() + getSpeed() * Math.sin(angle));
        this.setxCenter(getxCoord() + getWidth()/2);
        this.setyCenter(getyCoord() + getHeight()/2);
        return this;
    }

    public AbstractBee hiveDistanceUpdate() {
        setHiveDistance(getHiveDistance() + 0.01);
        return this;
    }

    @Override
    public Rectangle2D getBoundary()
    {
        return new Rectangle2D(this.getxCoord(), this.getyCoord(), this.getWidth(), this.getHeight());
    }

    @Override
    public String toString() {
        return "Bee{" +
                "HiveDistance = " + getHiveDistance() +
                '}';
    }

    @Override
    public AbstractBee transmitIfAvailable(List<AbstractBee> bees) {
        for (int i = 0; i < bees.size(); i++) {
            this.transmit(bees.get(i));
        }
        return this;
    }

    private double transmit(AbstractBee checkingBee) {
        if (isInTransmitRadius(checkingBee)) {
            if (checkingBee instanceof Bee) {
                if (((Bee) checkingBee).goToHiveWithColor == this.goToHiveWithColor) {
                    if (checkingBee.getHiveDistance() > (this.getHiveDistance() + this.getTransmissionRadius())) {
                        if (SwarmIntelligence.getTime() > (((Bee) checkingBee).getHiveCollisionTime() + COLLISION_TIMEOUT) && (SwarmIntelligence.getTime() > ((this.getHiveCollisionTime() + COLLISION_TIMEOUT)))) {
                            if (this.getDirection() == checkingBee.getDirection()) {

                            } else if (this.getDirection() > checkingBee.getDirection()) {
                                if (this.getDirection() - checkingBee.getDirection() > 180) {
                                    checkingBee.setDirection(checkingBee.getDirection() - 10);
                                } else {
                                    checkingBee.setDirection(checkingBee.getDirection() + 10);
                                }
                            } else if (this.getDirection() < checkingBee.getDirection()) {
                                if (checkingBee.getDirection() - this.getDirection() > 180) {
                                    checkingBee.setDirection(checkingBee.getDirection() + 10);
                                } else {
                                    checkingBee.setDirection(checkingBee.getDirection() - 10);
                                }
                            }
                            checkingBee.setHiveDistance(this.getHiveDistance() + this.getTransmissionRadius() + 1);
                        }
                    }
                }
            }
        }
        return this.getHiveDistance();
    }

    private double getDistanceBetweenBees(AbstractBee checkingBee) {
        return ((Math.sqrt(Math.pow(this.getxCenter() - checkingBee.getxCenter(), 2) + Math.pow(this.getyCenter() - checkingBee.getyCenter(), 2))));
    }

    private double getAngleBetweenBees(AbstractBee checkingBee) {
        double deltaX = this.getxCenter() - checkingBee.getxCenter();
        double deltaY = this.getyCenter() - checkingBee.getyCenter();
        double angle = Math.atan2(deltaX, deltaY);
        return Math.toDegrees(angle);
    }

    @Override
    public AbstractBee checkBorderCollision(double HEIGHT, double WIDTH) {
        double x = this.getxCoord();
        double y = this.getyCoord();
        if ((x > WIDTH - this.getWidth()) && (y > HEIGHT - this.getHeight()) ||
                (x < 0) && (y < 0) ||
                (x > WIDTH - this.getWidth()) && (y < 0) ||
                (x < 0) && (y > HEIGHT - this.getHeight())) {
            this.setDirection(180 - this.getDirection());
            this.setDirection(-this.getDirection());
        } else if ((x > WIDTH - this.getWidth()) || (x < 0)) {
            this.setDirection(180 - this.getDirection());
        } else if (y > HEIGHT - this.getHeight() || (y < 0)) {
            this.setDirection(-this.getDirection());
        }
        return this;
    }

    public double getHiveCollisionTime() {
        return hiveCollisionTime;
    }

    public void setHiveCollisionTime(double hiveCollisionTime) {
        this.hiveCollisionTime = hiveCollisionTime;
    }
}
