import javafx.geometry.Rectangle2D;
import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

public class Hive extends AbstractBee {

    double reduseSize = 0.0001;
    double colorIncrement = 0.02;

    public Hive(double xCoord, double yCoord, double speed, double direction, Color color) {
        super(xCoord, yCoord, speed, direction, color);
        this.setWidth(60);
        this.setHeight(60);
        this.setTransmissionRadius(30.0);
    }

    @Override
    public AbstractBee positionUpdate() {
        double angle = getDirection() * Math.PI / 180;
        this.setxCoord(getxCoord() + getSpeed() * Math.cos(angle));
        this.setyCoord(getyCoord() + getSpeed() * Math.sin(angle));
        this.setxCenter(getxCoord() + getWidth()/2);
        this.setyCenter(getyCoord() + getHeight()/2);
        return this;
    }

    @Override
    public AbstractBee hiveDistanceUpdate() {
        return this;
    }

    @Override
    public Rectangle2D getBoundary() {
        return new Rectangle2D(this.getxCoord(), this.getyCoord(), this.getWidth(), this.getHeight());
    }

    @Override
    public AbstractBee transmitIfAvailable(List<AbstractBee> bees) {
        for (int i = 0; i < bees.size(); i++) {
            this.transmit(bees.get(i));
        }
        return this;
    }

    public ArrayList<Color> getAllHiveColor(ArrayList<Hive> hives) {
        ArrayList<Color> colors = new ArrayList<>();
        for (Hive hive : hives) {
            colors.add(hive.getColor());
        }
        return colors;
    }

    private double transmit(AbstractBee checkingBee) {
        if (this.isInTransmitRadius(checkingBee)) {
            if (checkingBee instanceof Bee) {
                this.setHiveDistance(checkingBee.getHiveDistance());
                checkingBee.setHiveDistance(0);
                ArrayList<Color> hivesColors = getAllHiveColor(SwarmIntelligence.hives);
                for (int i = 0; i <= hivesColors.size() - 1; i++) {
                    if (((Bee) checkingBee).goToHiveWithColor.equals(hivesColors.get(i))) {
                        if (i == hivesColors.size() - 1) {
                            ((Bee) checkingBee).goToHiveWithColor = hivesColors.get(0);
                        } else {
                            ((Bee) checkingBee).goToHiveWithColor = hivesColors.get(i + 1);
                        }
                        break;
                    }
                }
                checkingBee.setColor(changeColor(checkingBee, colorIncrement));
                checkingBee.setDirection(checkingBee.getDirection() - 180);
                ((Bee) checkingBee).setHiveCollisionTime(SwarmIntelligence.getTime());
                this.setHeight(this.getHeight() - reduseSize);
                this.setWidth(this.getWidth() - reduseSize);
                this.setTransmissionRadius(this.getTransmissionRadius() - reduseSize / 2);
            }
        }
        return this.getHiveDistance();
    }

    private Color changeColor(AbstractBee bee, double colorIncrement) {
        double newRed = bee.getColor().getRed();
        double newGreen = bee.getColor().getBlue();
        double newBlue = bee.getColor().getGreen();
        double red = bee.getColor().getRed() - colorIncrement;
        double green = bee.getColor().getBlue() - colorIncrement;
        double blue = bee.getColor().getGreen() + colorIncrement;
        if (red <= 1 && red >= 0) {
            newRed = red;
        }
        if (green <= 1 && green >= 0) {
            newGreen = green;
        }
        if (blue <= 1 && blue >= 0) {
            newBlue = blue;
        }
        return new Color(newRed, newGreen, newBlue, 1);
    }

    @Override
    public AbstractBee checkBorderCollision(double HEIGHT, double WIDTH) {
        double x = this.getxCoord();
        double y = this.getyCoord();
        if ((x > WIDTH - this.getWidth()) && (y > HEIGHT - this.getHeight()) ||
                (x < 0) && (y < 0) ||
                (x > WIDTH - this.getWidth()) && (y < 0) ||
                (x < 0) && (y > HEIGHT - this.getHeight())) {
            this.setDirection(180 - this.getDirection());
            this.setDirection(-this.getDirection());
        } else if ((x > WIDTH - this.getWidth()) || (x < 0)) {
            this.setDirection(180 - this.getDirection());
        } else if (y > HEIGHT - this.getHeight() || (y < 0)) {
            this.setDirection(-this.getDirection());
        }
        return this;
    }
}
