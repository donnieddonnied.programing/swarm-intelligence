import javafx.scene.paint.Color;

import java.util.Random;

public class RandomGenerator {

    public static double getRandomDouble(double rangeMin, double rangeMax) {
        Random random = new Random();
        return rangeMin + (rangeMax - rangeMin) * random.nextDouble();
    }

    public static int getRandomInt(int rangeMax) {
        Random random = new Random();
        return random.nextInt(rangeMax+1);
    }

    public static double getRandomCoord(double rangeMax) {
        Random random = new Random();
        return rangeMax * random.nextDouble();
    }

    public static double getRandomDegree() {
        return getRandomDouble(0, 360);
    }

    public static Color getRandomColor() {
        double red = getRandomDouble(0, 1);
        double green = getRandomDouble(0, 1);
        double blue = getRandomDouble(0, 1);
        return new Color(red, green, blue, 1);
    }

}
