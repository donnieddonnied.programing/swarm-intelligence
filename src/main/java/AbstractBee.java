import javafx.geometry.Rectangle2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.List;

public abstract class AbstractBee {
    protected double height = 3;
    protected double width = 3;
    protected double transmissionRadius = 10;
    protected double xCoord;
    protected double yCoord;
    protected double xCenter;
    protected double yCenter;
    protected double speed;
    protected double direction;
    protected Color color;
    private double hiveDistance = RandomGenerator.getRandomInt(1000);

    public AbstractBee(double xCoord, double yCoord, double speed, double direction, Color color) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.speed = speed;
        this.direction = direction;
        this.color = color;
    }

    public abstract AbstractBee positionUpdate();

    public abstract AbstractBee hiveDistanceUpdate();

    public abstract Rectangle2D getBoundary();

    public AbstractBee render(GraphicsContext gc) {
        gc.setFill(this.getColor());
        gc.fillOval(this.getxCoord(), this.getyCoord(), this.getWidth() , this.getWidth());
        return this;
    }

    public abstract AbstractBee transmitIfAvailable(List<AbstractBee> bees);

    public abstract AbstractBee checkBorderCollision(double HEIGHT, double WIDTH);

    protected boolean isInTransmitRadius(AbstractBee checkingBee) {
        if (checkingBee.equals(this)) return false;
        return Math.pow((checkingBee.getxCenter() - this.getxCenter()), 2) + Math.pow((checkingBee.getyCenter() - this.getyCenter()), 2) < Math.pow(this.getTransmissionRadius(), 2);
    }

    public double getxCoord() {
        return xCoord;
    }

    public double getxCenter() {
        return xCenter;
    }

    public double getyCenter() {
        return yCenter;
    }

    public double getyCoord() {
        return yCoord;
    }

    public double getHeight() {
        return height;
    }
    public double getWidth() {
        return width;
    }

    public double getSpeed() {
        return speed;
    }

    public double getDirection() {
        return direction;
    }

    public Color getColor() {
        return color;
    }

    public double getHiveDistance() {
        return hiveDistance;
    }

    public double getTransmissionRadius() {
        return transmissionRadius;
    }

    public void setHiveDistance(double hiveDistance) {
        this.hiveDistance = hiveDistance;
    }
    public void setxCenter(double xCenter) {
        this.xCenter = xCenter;
    }

    public void setyCenter(double yCenter) {
        this.yCenter = yCenter;
    }

    public void setxCoord(double xCoord) {
        this.xCoord = xCoord;
    }

    public void setyCoord(double yCoord) {
        this.yCoord = yCoord;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double setTransmissionRadius(Double transmissionRadius) {
        return this.transmissionRadius = transmissionRadius;
    }

    public void setDirection(double direction) {
        if (direction >= 0 && direction <= 360) {
            this.direction = direction;
        } else if (direction < 0) {
            setDirection(direction + 360);
        } else if (direction > 360) {
            setDirection(direction - 360);
        }
    }
}
